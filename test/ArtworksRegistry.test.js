const ArtworksRegistry = artifacts.require('ArtworksRegistry');

async function acatch(fn) {
  try {
    const res = await fn();
    return [null, res];
  } catch (e) {
    return [e, null];
  }
}

contract('1st ArtworksRegistry test', async (accounts) => {
  let defaultTokenId = 1337;
  let url = `http://localhost:8080/artworks/1.json`;

  it('should be empty', async () => {
    // Arrange
    // Act
    let instance = await ArtworksRegistry.deployed();
    let entities = await instance.totalSupply();
    // Assert
    assert.equal(entities.toString(), '0');
  });

  it('should throw an error', async () => {
    // Arrange
    let instance = await ArtworksRegistry.deployed();
    // Act
    const [e, entities] = await acatch(() => instance._exists(123456));
    // Assert
    assert.isOk(!!e, 'Token not in contract');
  });

  it('should have only one', async () => {
    // Arrange
    let instance = await ArtworksRegistry.deployed();

    // Act
    await instance.mint(defaultTokenId, accounts[1], url);

    // Assert
    const saved_url = await instance.tokenURI(defaultTokenId);
    let owner = await instance.ownerOf(defaultTokenId);

    assert.equal(owner.toString(), accounts[1]);
    assert.equal(saved_url, url);
  });

  it('contractURI', async () => {
    let instance = await ArtworksRegistry.deployed();
    const contractURI = await instance.contractURI();

    assert.equal(contractURI, 'https://gitlab.com/stendhal-tech/artists-artworks-registries/-/raw/main/public/artworks-contract.json');
  });
});
