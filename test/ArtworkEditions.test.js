const ArtworkEditions = artifacts.require('ArtworkEditions');

async function acatch(fn) {
  try {
    const res = await fn();
    return [null, res];
  } catch (e) {
    return [e, null];
  }
}

contract('1st ArtworkEditions test', async (accounts) => {
  let defaultTokenId = 1337;
  let url = `http://localhost:8080/editions/1.json`;

  it('should throw an error', async () => {
    // Arrange
    let instance = await ArtworkEditions.deployed();
    // Act
    const [e, entities] = await acatch(() => instance._exists(123456));
    // Assert
    assert.isOk(!!e, 'Token not in contract');
  });

  it('should have only one', async () => {
    // Arrange
    let instance = await ArtworkEditions.deployed();

    // Act
    const { tx } = await instance.mint(accounts[1], defaultTokenId, 10, []);

    // Assert
    assert.equal(10, await instance.balanceOf(accounts[1], defaultTokenId));

    // TODO test event
  });

  it('isApproved', async () => {
    let instance = await ArtworkEditions.deployed();
    await instance.setApprovalForAll(accounts[1], true, { from: accounts[3] });

    assert.isOk(await instance.isApprovedForAll(accounts[3], accounts[1]));
  });

  it.skip('setURI', async () => {
    // Arrange
    let instance = await ArtworkEditions.deployed();
    const uri = await instance.uri(123);

    await instance.setURI('http://localhost:8080/{id}');

    const newUri = await instance.uri(123);
    assert.notEqual(uri, newUri);
    assert.equal(newUri, 'http://localhost:8080/123');
  });

  it('contractURI', async () => {
    let instance = await ArtworkEditions.deployed();
    const contractURI = await instance.contractURI();

    assert.equal(contractURI, 'https://gitlab.com/stendhal-tech/artists-artworks-registries/-/raw/main/public/artwork-editions-contract.json');
  });
});

contract('ArtworkEditions safeBatchTransferIdFrom', async (accounts) => {
  it('safeBatchTransferIdFrom successful', async () => {
    // Arrange
    let instance = await ArtworkEditions.deployed();
    let id = 53;
    let editions = 10;
    let to = [accounts[2], accounts[3], accounts[5]];
    let amounts = [2, 4, 1];
    await instance.mint(accounts[1], id, editions, []);
    // Act
    await instance.safeBatchTransferIdFrom(accounts[1], to, id, amounts, [], { from: accounts[1] });

    // Assert
    assert.equal(2, await instance.balanceOf(accounts[2], id));
    assert.equal(4, await instance.balanceOf(accounts[3], id));
    assert.equal(1, await instance.balanceOf(accounts[5], id));

    assert.equal(0, await instance.balanceOf(accounts[4], id));
    assert.equal(0, await instance.balanceOf(accounts[6], id));
    assert.equal(3, await instance.balanceOf(accounts[1], id));
  });

  it('safeBatchTransferIdFrom bad amounts', async () => {
    // Arrange
    let instance = await ArtworkEditions.deployed();
    let id = 53;
    let editions = 10;
    let to = [accounts[2]];
    let amounts = [2, 4, 1];

    // Act
    const [e, entities] = await acatch(() => instance.safeBatchTransferIdFrom(accounts[1], to, id, amounts, []));

    // Assert
    assert.isOk(!!e, 'ERC1155: to and amounts length mismatch');
  });

  it('safeBatchTransferIdFrom caller not owner', async () => {
    // Arrange
    let instance = await ArtworkEditions.deployed();
    let id = 53;
    let editions = 10;
    let to = [accounts[2], accounts[3], accounts[5]];
    let amounts = [2, 4, 1];
    await instance.mint(accounts[1], id, editions, []);

    // Act
    const [e, entities] = await acatch(() => instance.safeBatchTransferIdFrom(accounts[8], to, id, amounts, [], { from: accounts[7] }));

    // Assert
    assert.isOk(!!e, 'ERC1155: caller is not owner nor approved.');
  });

  it('safeBatchTransferIdFrom caller not owner (deployer)', async () => {
    // Arrange
    let instance = await ArtworkEditions.deployed();
    let id = 53;
    let editions = 10;
    let to = [accounts[2], accounts[3], accounts[5]];
    let amounts = [2, 4, 1];
    await instance.mint(accounts[1], id, editions, []);

    // Act
    const [e, entities] = await acatch(() => instance.safeBatchTransferIdFrom(accounts[8], to, id, amounts, []));

    // Assert
    assert.isOk(!!e, 'ERC1155: caller is not owner nor approved.');
  });
});

contract('ArtworkEditions safeBatchTransferIdsFrom', async (accounts) => {
  it('safeBatchTransferIdsFrom to and amounts length mismatch', async () => {
    // Arrange
    let instance = await ArtworkEditions.deployed();
    let id = [53, 54, 55];
    let editions = 10;
    let to = [accounts[2]];
    let amounts = [2, 4, 1];

    // Act
    const [e, entities] = await acatch(() => instance.safeBatchTransferIdFrom(accounts[1], to, id, amounts, []));

    // Assert
    assert.isOk(!!e, 'ERC1155: to and amounts length mismatch');
  });

  it('safeBatchTransferIdsFrom ids and amounts length mismatch', async () => {
    // Arrange
    let instance = await ArtworkEditions.deployed();
    let id = [53];
    let editions = 10;
    let to = [accounts[2]];
    let amounts = [2, 4, 1];

    // Act
    const [e, entities] = await acatch(() => instance.safeBatchTransferIdsFrom(accounts[1], to, id, amounts, []));

    // Assert
    assert.isOk(!!e, 'ERC1155: ids and amounts length mismatch');
  });

  it('safeBatchTransferIdsFrom successful', async () => {
    // Arrange
    let instance = await ArtworkEditions.deployed();
    let id = [53, 54, 55];
    let editions = 10;
    let to = [accounts[2], accounts[3], accounts[5]];
    let amounts = [1, 2, 3];
    await instance.mint(accounts[1], id[0], editions, []);
    await instance.mint(accounts[1], id[1], editions, []);
    await instance.mint(accounts[1], id[2], editions, []);

    // Act
    await instance.safeBatchTransferIdsFrom(accounts[1], to, id, amounts, [], { from: accounts[1] });

    // Assert
    assert.equal(1, await instance.balanceOf(accounts[2], id[0]));
    assert.equal(2, await instance.balanceOf(accounts[3], id[1]));
    assert.equal(3, await instance.balanceOf(accounts[5], id[2]));
    
    assert.equal(9, await instance.balanceOf(accounts[1], id[0]));
    assert.equal(8, await instance.balanceOf(accounts[1], id[1]));
    assert.equal(7, await instance.balanceOf(accounts[1], id[2]));

    assert.equal(0, await instance.balanceOf(accounts[2], id[1]));
    assert.equal(0, await instance.balanceOf(accounts[2], id[2]));
    assert.equal(0, await instance.balanceOf(accounts[3], id[0]));
    assert.equal(0, await instance.balanceOf(accounts[3], id[2]));
    assert.equal(0, await instance.balanceOf(accounts[5], id[0]));
    assert.equal(0, await instance.balanceOf(accounts[5], id[1]));

    assert.equal(0, await instance.balanceOf(accounts[4], id[0]));
  });
});