const ArtistsRegistry = artifacts.require('ArtistsRegistry');

async function acatch(fn) {
  try {
    const res = await fn();
    return [null, res];
  } catch (e) {
    return [e, null];
  }
}

contract('1st ArtistsRegistry test', async (accounts) => {
  let url = `http://localhost:8080/${accounts[0]}.json`;
  let url1 = `http://localhost:8080/${accounts[1]}.json`;

  it('should be empty', async () => {
    let instance = await ArtistsRegistry.deployed();
    let entities = await instance.totalEntities();
    assert.equal(entities.toString(), '0');
  });

  it('should throw an error', async () => {
    let instance = await ArtistsRegistry.deployed();
    const [e, entities] = await acatch(() => instance.getByAddress(accounts[0]));
    assert.isOk(!!e, 'Address not in contract');
  });

  it('should have only one', async () => {
    let instance = await ArtistsRegistry.deployed();
    await instance.register(url);
    const saved_url = await instance.getByIndex(0);
    assert.equal(saved_url, url);
  });

  it('should be validated to 1', async () => {
    let instance = await ArtistsRegistry.deployed();
    await instance.validateEntity(accounts[0], 1);
    const validationLevel = (await instance.getValidationLevel(accounts[0])).toString();
    assert.equal(+validationLevel, 1);
  });

  it('should be transfered to another address', async () => {
    let instance = await ArtistsRegistry.deployed();
    await instance.transferAddress(accounts[1]);

    const [e, oldEntity] = await acatch(() => instance.getByAddress(accounts[0]));
    assert.isOk(!!e, 'Old entity is not registered anymore');
    const [e2, newEntity] = await acatch(() => instance.getByAddressWithValidation(accounts[1]));
    assert.isOk(!e2, 'New entity exists');
    assert.equal(newEntity[0], url, 'New entity url is set');
    assert.equal(newEntity[1].toString(), '1', 'New entity validationLevel is 1');
  });

  it('should register the given address and uri', async () => {
    let instance = await ArtistsRegistry.deployed();
    const [e, result] = await acatch(() => instance.registerUser(accounts[1], url1));

    const [e2, newEntity] = await acatch(() => instance.getByAddress(accounts[1]));
    assert.isOk(!e2, 'New entity exists');
    assert.equal(newEntity, url1, 'New entity url is set');
  });

  it('contractURI', async () => {
    let instance = await ArtistsRegistry.deployed();
    const contractURI = await instance.contractURI();

    assert.equal(contractURI, 'https://gitlab.com/stendhal-tech/artists-artworks-registries/-/raw/main/public/artists-contract.json');
  });
});
