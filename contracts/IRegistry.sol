// SPDX-License-Identifier: MIT
pragma solidity ^0.6.2;

interface IRegistry {
    /**
     * @dev Emitted when `user` register a new `uri`.
     */
    event Register(address indexed user, string uri, uint256 index);

    /**
     * @dev Emitted when `user` unregisters from registry.
     */
    event Unregister(address indexed user);


    function schema() external view returns (string memory);

    function uri(address _address) external view returns (string memory);

    function totalEntities() external view returns (uint256);

    function getAddress(uint256 _index) external view returns (address);

    function getByAddress(address _entity) external view returns (string memory);

    function getByIndex(uint256 _index) external view returns (string memory);

    function register(string calldata _uri) external;

    function unregister() external;

    function transferAddress(address _to) external;
}