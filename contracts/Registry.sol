// SPDX-License-Identifier: MIT
pragma solidity ^0.6.2;

import "@openzeppelin/contracts/utils/EnumerableSet.sol";
import "@openzeppelin/contracts/GSN/Context.sol";
import "./IRegistry.sol";

contract Registry is Context, IRegistry {
    using EnumerableSet for EnumerableSet.AddressSet;

    // all registered address
    EnumerableSet.AddressSet private _entities;

    // mapping for address URIs
    mapping(address => string) private _entitiesURIs;

    string private _entitySchema;
    string private _URI;

    constructor(string memory _schema, string memory _uri) public {
        // set schema uri
        _setSchema(_schema);
        _setURI(_uri);
    }

    function _setSchema(string memory _schema) internal {
        _entitySchema = _schema;
    }

    function schema() public override view returns (string memory) {
        return _entitySchema;
    }

    function _setURI(string memory _uri) internal virtual {
        _URI = _uri;
    }

    /**
     * If `\{address\}` is in URI, cients calling this function must
     * replace the `\{address\}` substring with the actual entity address.
     */
    function uri(address _address) public override view returns (string memory) {
        bytes memory tempEmptyStringTest = bytes(_entitiesURIs[_address]);
        if (tempEmptyStringTest.length != 0) {
            return _entitiesURIs[_address];
        }

        return _URI;
    }


    // tells if a entity is already registered
    function _exists(address _entity) internal view returns (bool) {
        return _entities.contains(_entity);
    }

    function _remove(address _entity) internal returns (bool) {
        return _entities.remove(_entity);
    }

    /**
     * Called by an artist to register the json schema associated to their address
     */
    function _register(address _entity, string memory _uri) internal virtual {
        _entities.add(_entity);
        _entitiesURIs[_entity] = _uri;

        emit Register(_entity, _uri, totalEntities());
    }

    function _unregister(address _entity) internal virtual {
        if (_remove(_entity)) {
            _entitiesURIs[_entity] = '';
            emit Unregister(_entity);
        }
    }

    function _transferAddress(address _from, address _to) internal virtual {
        _register(_to, _entitiesURIs[_from]);
        _unregister(_from);
    }

    function register(string memory _uri) public override {
        _register(_msgSender(), _uri);
    }

    function unregister() public override {
        _unregister(_msgSender());
    }

    function transferAddress(address _to) public virtual override {
        _transferAddress(_msgSender(), _to);
    }

    function totalEntities() public override view returns (uint256) {
        return _entities.length();
    }



    function getAddress(uint256 _index) public override view returns (address) {
        return _entities.at(_index);
    }

    function getByAddress(address _entity) public override view returns (string memory) {
        require(_exists(_entity), "Unregistered entity");
        return uri(_entity);
    }

    function getByIndex(uint256 _index) public override view returns (string memory) {
        return getByAddress(getAddress(_index));
    }
}