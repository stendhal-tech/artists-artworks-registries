// SPDX-License-Identifier: MIT
pragma solidity ^0.6.2;

interface IArtistsRegistry {
    function doesRevertAgain(address _to) external;
}