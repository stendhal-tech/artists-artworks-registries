// SPDX-License-Identifier: MIT
pragma solidity ^0.6.2;

import "./Registry.sol";
import "./IValidableRegistry.sol";

contract ValidableRegistry is Registry, IValidableRegistry {
    // mapping for entityes validation
    mapping(address => uint8) private _validations;

    constructor(string memory _schema, string memory _uri) public Registry(_schema, _uri) {}

    function _validateEntity(address _validator, address _entity, uint8 _level) public {
        require(_exists(_entity), "Entity is not registered");
        _validations[_entity] = _level;

        emit Validation(_validator, _entity, _level);
    }

    function _validateBatch(address _validator, address[] calldata _entities, uint8[] calldata _levels) internal {
        require(_entities.length > 0, "Validation: Nothing to validate");
        require(_entities.length == _levels.length, "Validation: Should have as much validation levels as entities");
        for(uint256 i = 0; i < _entities.length; i++) {
            require(_exists(_entities[i]), "Entity is not registered");
            _validations[_entities[i]] = _levels[i];
        }

        emit ValidationBatch(_validator, _entities, _levels);
    }

    function _unregister(address _entity) internal override {
        super._unregister(_entity);
        _validations[_entity] = 0;
    }

    function _transferAddress(address _from, address _to) internal override {
        _validations[_to] = _validations[_from];
        _validations[_from] = 0;
        super._transferAddress(_from, _to);
    }

    function validateBatch(address[] calldata _entities, uint8[] calldata _levels) external override {
        _validateBatch(_msgSender(), _entities, _levels);
    }

    function validateEntity(address _entity, uint8 _level) public virtual override {
        _validateEntity(_msgSender(), _entity, _level);
    }

    function isValidated(address _entity) public view override returns (bool) {
        require(_exists(_entity), "Unregistered entity");
        return _validations[_entity] > 0;
    }

    function getValidationLevel(address _entity) public view override returns (uint8) {
        require(_exists(_entity), "Unregistered entity");
        return _validations[_entity];
    }

    function getByAddressWithValidation(address _entity) public view override(IValidableRegistry) returns (string memory, uint8) {
        require(_exists(_entity), "Unregistered entity");
        return (uri(_entity), _validations[_entity]);
    }

    function getByIndexWithValidation(uint256 _index) public view override(IValidableRegistry) returns (string memory, address, uint8) {
        address _address = getAddress(_index);
        (string memory uri, uint8 validation) = getByAddressWithValidation(_address);
        return (uri, _address,  validation);
    }
}