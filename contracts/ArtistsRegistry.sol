// SPDX-License-Identifier: MIT
pragma solidity ^0.6.2;

import "@openzeppelin/contracts/access/AccessControl.sol";
import "./ValidableRegistry.sol";

contract ArtistsRegistry is AccessControl, ValidableRegistry {
    bytes32 public constant VALIDATOR_ROLE = keccak256("VALIDATOR_ROLE");

    constructor(string memory _schema, string memory _uri) public ValidableRegistry(_schema, _uri) {
        // Grant the contract deployer the default admin role: it will be able
        // to grant and revoke any roles
        _setupRole(DEFAULT_ADMIN_ROLE, _msgSender());
        _setupRole(VALIDATOR_ROLE, _msgSender());
    }

    function contractURI() public view returns (string memory) {
        return "https://gitlab.com/stendhal-tech/artists-artworks-registries/-/raw/main/public/artists-contract.json";
    }

    function setSchema(string memory _schema) public {
        require(hasRole(DEFAULT_ADMIN_ROLE, _msgSender()), "Caller is not an admin");
        _setSchema(_schema);
    }

    /**
     * ADMIN / Validators
     */
    function registerUser(address _entity, string memory _uri) external {
        require(hasRole(VALIDATOR_ROLE, _msgSender()), "Caller is not a validator");
        _register(_entity, _uri);
    }

    function registerUser(address _entity, string memory _uri, uint8 _level) external {
        require(hasRole(VALIDATOR_ROLE, _msgSender()), "Caller is not a validator");
        _register(_entity, _uri);
        _validateEntity(_msgSender(), _entity, _level);
    }

    function unregisterUser(address _entity) external {
        require(hasRole(VALIDATOR_ROLE, _msgSender()), "Caller is not a validator");
        _unregister(_entity);
    }

    function validateEntity(address _entity, uint8 _level) public override {
        require(hasRole(VALIDATOR_ROLE, _msgSender()), "Caller is not a validator");
        _validateEntity(_msgSender(), _entity, _level);
    }

    function transferAddress(address _from, address _to) public {
        require(hasRole(VALIDATOR_ROLE, _msgSender()), "Caller is not a validator");
        require(_exists(_from), "From address not registered");
        require(!_exists(_to), "Destination address is already registered");

        _transferAddress(_from, _to);
    }

    /**
     * Public access
     */
    function transferAddress(address _to) public override {
        require(!_exists(_to), "Destination address is already registered");
        require(_exists(_msgSender()), "Caller not registered");

        _transferAddress(_msgSender(), _to);
    }

    /**
     * Accessors
     */
    function getBatchAddress(uint256[] memory _indexes) public view returns (address[] memory) {
        address[] memory entities;
        for(uint256 i = 0; i < _indexes.length; i++) {
            entities[i] = super.getAddress(_indexes[i]);
        }

        return entities;
    }
}