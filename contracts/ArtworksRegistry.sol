// SPDX-License-Identifier: MIT
pragma solidity ^0.6.2;

import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "@openzeppelin/contracts/access/Ownable.sol";

/**
 * @dev Artworks registry (ERC721 non-fungible token standard) to represent moral rights for an artwork.
 */
contract ArtworksRegistry is ERC721, Ownable {

    /**
     * @dev Emitted when `artist` mint a new artwork in the registry, at `index` with more info at `uri`.
     */
    event Mint(address indexed user, uint256 index, string uri);

    constructor() public ERC721("ArtworksRegistry", "A5K") {
        // _setBaseURI("https://api.wallkanda.art/artworks/{id}");
    }

    function contractURI() public view returns (string memory) {
        return "https://gitlab.com/stendhal-tech/artists-artworks-registries/-/raw/main/public/artworks-contract.json";
    }

    /**
    * @dev Mint a new artwork owned by the caller.
    * @param _tokenURI URI of the artwork's DID Document
    */
    function mint(uint256 _tokenId, address _to, string memory _tokenURI) public onlyOwner {
        _safeMint(_to, _tokenId);
        _setTokenURI(_tokenId, _tokenURI);

        emit Mint(_to, _tokenId, _tokenURI);
    }
}