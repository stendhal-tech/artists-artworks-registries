// SPDX-License-Identifier: MIT
pragma solidity ^0.6.2;

import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/token/ERC1155/ERC1155.sol";

contract ArtworkEditions is ERC1155, Ownable {

    constructor() public ERC1155('https://res.wallkanda.art/v0/editions/{id}') {
    }

    function contractURI() public view returns (string memory) {
        return "https://gitlab.com/stendhal-tech/artists-artworks-registries/-/raw/main/public/artwork-editions-contract.json";
    }

    function mint(address _to, uint256 _id, uint256 _amount, bytes calldata _data) public onlyOwner {
        _mint(_to, _id, _amount, _data);
    }

    function mintBatch(address _to, uint256[] calldata _ids, uint256[] calldata _amounts, bytes calldata _data) public onlyOwner {
        _mintBatch(_to, _ids, _amounts, _data);
    }

    /**
     * @dev allows to transfer one id to several recipient with corresponding amounts
     */
    function safeBatchTransferIdFrom(
        address from,
        address[] memory to,
        uint256 id,
        uint256[] memory amounts,
        bytes memory data
    ) public virtual {
        require(
            to.length == amounts.length,
            'ERC1155: to and amounts length mismatch'
        );
        for (uint256 i = 0; i < to.length; i++) {
            safeTransferFrom(from, to[i], id, amounts[i], data);
        }
    }

    /**
     * @dev allows to transfer several ids to several recipient with corresponding amounts i.e. do the transfert for each truple (id, to, amount)
     */
    function safeBatchTransferIdsFrom(
        address from,
        address[] memory to,
        uint256[] memory ids,
        uint256[] memory amounts,
        bytes memory data
    ) public virtual {
        require(
            to.length == amounts.length,
            'ERC1155: to and amounts length mismatch'
        );
        require(
            ids.length == amounts.length,
            'ERC1155: ids and amounts length mismatch'
        );
        for (uint256 i = 0; i < to.length; i++) {
            safeTransferFrom(from, to[i], ids[i], amounts[i], data);
        }
    }

    function setURI(string memory newuri) onlyOwner external {
        _setURI(newuri);
    }
}