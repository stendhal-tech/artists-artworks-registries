// SPDX-License-Identifier: MIT
pragma solidity ^0.6.2;

interface IValidableRegistry {
    /**
     * @dev Emitted when `validator` validates a `user` to `level`.
     */
    event Validation(address indexed validator, address indexed entitiy, uint8 indexed level);

    /**
     * @dev Emitted when `validator` validates `artists` to `levels`.
     */
    event ValidationBatch(address indexed validator, address[] entities, uint8[] levels);

    function validateEntity(address _entity, uint8 _level) external;

    function validateBatch(address[] calldata _entities, uint8[] calldata _levels) external;

    function isValidated(address _entity) external view returns (bool);

    function getValidationLevel(address _entity) external view returns (uint8);

    function getByAddressWithValidation(address _entity) external view returns (string memory, uint8);

    function getByIndexWithValidation(uint256 _index) external view returns (string memory, address, uint8);
}