const ArtistsRegistry = artifacts.require('ArtistsRegistry');
const ArtworksRegistry = artifacts.require('ArtworksRegistry');
const ArtworkEditions = artifacts.require('ArtworkEditions');

module.exports = async function (deployer) {
  await deployer.deploy(ArtistsRegistry, 'https://schema.org/Person', '');
  await deployer.deploy(ArtworksRegistry);
  await deployer.deploy(ArtworkEditions);
  const artistsRegistry = await ArtistsRegistry.deployed();
  const artworksRegistry = await ArtworksRegistry.deployed();
  const editions = await ArtworkEditions.deployed();
  // do stuff
};
